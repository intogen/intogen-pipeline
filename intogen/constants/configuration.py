

MAIN_SCRIPT_CONFIGURATION = {
    "pool_analysis_enabled": "boolean(default=False)",
    "pool_name": "string(default=all_projects)",
    "pool_projects": "string_list(default=list('.*'))",
    "mutsig_enabled": "boolean(default=False)",
    "kegg_path": "file",
    "gene_transcripts_path": "file"
}
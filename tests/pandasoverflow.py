import pandas as pd
import numpy as np

values = range(1, 2**16 + 10)
data = pd.DataFrame.from_dict({'a': values, 'b': values, 'c': values, 'd': values})
grouped = data.groupby(['a', 'b', 'c', 'd'])
len(grouped)
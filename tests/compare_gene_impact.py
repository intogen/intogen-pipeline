import csv
import os
from intogen.utils import CommentedFile
from tests.compare import diff_str, diff_sets, diff_double

new_file = "~/intogen5/bin/output/PRAD_broad/sample_gene.impact"
old_file = "~/intogen5/examples/projects/PRAD_broad/wok-output/results/gene_sample-fimpact.tsv"

new_genes = {}
with open(os.path.expanduser(new_file), "r") as f:
    reader = csv.DictReader(CommentedFile(f), delimiter='\t')

    new_count = 0
    duplicates_count = 0
    for o in reader:
        new_count += 1
        primary_key = ":".join([o['sample'], o['gene']])

        if primary_key in new_genes:
            duplicates_count += 1

        new_genes[primary_key] = o

with open(os.path.expanduser(old_file), "r") as f:
    reader = csv.DictReader(CommentedFile(f), delimiter='\t')

    old_keys = set()
    old_count = 0

    for o in reader:
        old_count += 1
        primary_key = ":".join([o['SAMPLE'], o['GENE_ID']])
        old_keys.add(primary_key)

        if not primary_key in new_genes:
            print("{0} not found in new file".format(primary_key))
            continue

        n = new_genes[primary_key]

        # SIFT
        if diff_double(o['SIFT_SCORE'], n['sift_score']):
            print("At {0} sift_score was #{1}# and now is #{2}#".format(primary_key, o['SIFT_SCORE'], n['sift_score']))

        if diff_double(o['SIFT_TRANSFIC'], n['sift_tfic']):
            print("At {0} sift_tfic was #{1}# and now is #{2}#".format(primary_key, o['SIFT_TRANSFIC'], n['sift_tfic']))

        if diff_str(o['SIFT_TRANSFIC_CLASS'], n['sift_class']):
            print("At {0} sift_class was #{1}# and now is #{2}#".format(primary_key, o['SIFT_TRANSFIC_CLASS'], n['sift_class']))

        # PPH2
        if diff_double(o['PPH2_SCORE'], n['pph2_score']):
            print("At {0} pph2_score was #{1}# and now is #{2}#".format(primary_key, o['PPH2_SCORE'], n['pph2_score']))

        if diff_double(o['PPH2_TRANSFIC'], n['pph2_tfic']):
            print("At {0} pph2_tfic was #{1}# and now is #{2}#".format(primary_key, o['PPH2_TRANSFIC'], n['pph2_tfic']))

        if diff_str(o['PPH2_TRANSFIC_CLASS'], n['pph2_class']):
            print("At {0} pph2_class was #{1}# and now is #{2}#".format(primary_key, o['PPH2_TRANSFIC_CLASS'], n['pph2_class']))

        # MA
        if diff_double(o['MA_SCORE'], n['ma_score']):
            print("At {0} ma_score was #{1}# and now is #{2}#".format(primary_key, o['MA_SCORE'], n['ma_score']))

        if diff_double(o['MA_TRANSFIC'], n['ma_tfic']):
            print("At {0} ma_tfic was #{1}# and now is #{2}#".format(primary_key, o['MA_TRANSFIC'], n['ma_tfic']))

        if diff_str(o['MA_TRANSFIC_CLASS'], n['ma_class']):
            print("At {0} ma_class was #{1}# and now is #{2}#".format(primary_key, o['MA_TRANSFIC_CLASS'], n['ma_class']))


for k in new_genes.keys():

    if not k in old_keys:
        print("{0} not found in old file".format(k))

print("Total new entries: {0}".format(new_count))
print("Total old entries: {0}".format(old_count))
print("Total duplicates: {0}".format(duplicates_count))




import pandas

input_file  = "/shared/projects/intogen/mutations/releases/v3/3.0-20141212/project/literature_SCLC_JHU/sample_variant+transcript.impact"

data_in = pandas.read_csv(input_file, sep='\t',  na_values="")
group_by = ["GENE", "TRANSCRIPT", "CHR", "STRAND", "START", "REF", "ALT", "PROTEIN_POS", "MOST_SEVERE"]
data_in = data_in[group_by]
consequences = ["missense_variant", "synonymous_variant", "stop_gained", "stop_lost", "frameshift_variant", "splice_donor_variant", "splice_acceptor_variant", "splice_region_variant", "initiator_codon_variant", "incomplete_terminal_codon_variant", "inframe_deletion", "inframe_insertion"]
data_in = data_in[data_in['MOST_SEVERE'].isin(consequences)]
#data_in = data_in[data_in['PROTEIN_POS'] != '999']

# OK
#d = data_in.head(25803)
# KO
d = data_in.head(25804)

# print(data_head.tail(1))

grouped = d.groupby(group_by)

print(len(grouped))

for k, gp in grouped:
    if (k[0]=='ENSG00000269741') and (k[1]=='ENST00000599166') and (k[4]==51499472) and (k[8]=='splice_acceptor_variant'):
        print('key={}'.format(k))

print("# Conflict protein pos")
print(d[(d.GENE=='ENSG00000269741') & (d.TRANSCRIPT=='ENST00000599166') & (d.START==51499472) & (d.MOST_SEVERE=='splice_acceptor_variant')])


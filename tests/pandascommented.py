import pandas as pd
import numpy as np
from intogen.utils import CommentedFile

# Create a random dataframe
df1 = pd.DataFrame(np.random.randn(6, 4), columns=list('ABCD'))

file = '/tmp/testpd.csv'
print(df1.head())
df1.to_csv(file, index=False)
df2 = pd.read_csv(file)
print(df2.head())
df3 = pd.read_csv(CommentedFile(file, 'r'))
print(df3.head())
df3.to_csv(CommentedFile(file, 'w', comments=["Hola"], values={'GENE': 'Gene id'}), index=False)

cf = CommentedFile(file, 'r')
df4 = pd.read_csv(cf)

print(df4.head())
print(cf.comments)
print(cf.values)








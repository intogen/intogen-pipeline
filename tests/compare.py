
def below_threshold(values, threshold=0.05, nulls=('\\N', '', 'NA', '-')):

    f_values = [float(v) < threshold for v in values if v not in nulls]
    return any(f_values)

def diff_double(src, dst, delta=0.0001, nulls=('\\N', '', 'NA', '-')):

    src = None if src in nulls else src
    dst = None if dst in nulls else dst

    if src is None and dst is not None:
        return True

    if dst is None and src is not None:
        return True

    if dst is None and src is None:
        return False

    d_src = float(src)
    d_dst = float(dst)

    diff = abs(d_src - d_dst)

    return diff > delta


def diff_str(src, dst, nulls=('\\N', '', 'NA')):

    src = None if src in nulls else src
    dst = None if dst in nulls else dst

    return src != dst


def diff_sets(src, dst, separator=","):
    ss = src.split(separator)
    dd = dst.split(separator)

    for s in ss:
        if not s in dd:
            return True

    for d in dd:
        if not d in ss:
            return True

    return False

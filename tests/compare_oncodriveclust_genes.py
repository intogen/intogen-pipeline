import csv
import os
from intogen.utils import CommentedFile
from tests.compare import diff_str, diff_sets, diff_double

new_file = "~/intogen5/bin/output/PRAD_broad/gene.oncodriveclust"
old_file = "~/intogen5/examples/projects/PRAD_broad/wok-output/temp/oncodriveclust-results.tsv"

new_genes = {}
with open(os.path.expanduser(new_file), "r") as f:
    reader = csv.DictReader(CommentedFile(f), delimiter='\t')

    new_count = 0
    duplicates_count = 0
    for o in reader:
        new_count += 1
        primary_key = ":".join([o['GENE']])

        if primary_key in new_genes:
            duplicates_count += 1

        new_genes[primary_key] = o

with open(os.path.expanduser(old_file), "r") as f:
    reader = csv.DictReader(CommentedFile(f), delimiter='\t')

    old_keys = set()
    old_count = 0

    for o in reader:
        old_count += 1
        primary_key = ":".join([o['GENE']])
        old_keys.add(primary_key)

        if not primary_key in new_genes:
            print("{0} not found in new file".format(primary_key))
            continue

        n = new_genes[primary_key]

        if diff_double(o['PVALUE'], n['PVALUE']):
            print("At {0} pvalue was #{1}# and now is #{2}#".format(primary_key, o['PVALUE'], n['PVALUE']))

        if diff_double(o['QVALUE'], n['QVALUE']):
            print("At {0} qvalue was #{1}# and now is #{2}#".format(primary_key, o['QVALUE'], n['QVALUE']))

        if diff_double(o['ZSCORE'], n['ZSCORE']):
            print("At {0} zscore was #{1}# and now is #{2}#".format(primary_key, o['ZSCORE'], n['ZSCORE']))

for k in new_genes.keys():

    if not k in old_keys:
        print("{0} not found in old file".format(k))

print("Total new entries: {0}".format(new_count))
print("Total old entries: {0}".format(old_count))
print("Total duplicates: {0}".format(duplicates_count))




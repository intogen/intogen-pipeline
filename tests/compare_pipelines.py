import csv
import os
import sys
from tests.compare import diff_double, below_threshold

new_file = "intogen3_all_pvalues.tsv"
old_file = "intogen2_all_pvalues.tsv"

if len(sys.argv) > 1:
    old_file = sys.argv[1]

if len(sys.argv) > 2:
    new_file = sys.argv[2]

CLUST_OLD = 'CLUST_PVALUE'
CLUST_NEW = 'PVALUE_ONCODRIVECLUST'
FM_OLD = 'FM_PVALUE'
FM_NEW = 'PVALUE_ONCODRIVEFM'

different_count = {'FM': 0, 'CLUST': 0}
similar_count = {'FM': 0, 'CLUST': 0}

sym_dict = {}

only_low_ps = True
P = 0.05

skipped_projects = ["tcga_pancancer", "PRAD_broad","mm_broad", "literature_CLL_DF", "DLBCL_broad","Astrocytoma_StJude"]

def get_sym(key):
    if key in sym_dict:
        return sym_dict[key]
    return ""

new_genes = {}
with open(os.path.expanduser(new_file), "r") as f:
    reader = csv.DictReader(f, delimiter='\t')

    new_count = 0
    duplicates_count = 0
    for o in reader:
        if o['PROJECT'] in skipped_projects:
            continue
        new_count += 1
        primary_key = ":".join([o['GENE'], o['PROJECT']])

        if primary_key in new_genes:
            duplicates_count += 1

        new_genes[primary_key] = o

        if 'SYMBOL' in o and o['SYMBOL'] not in sym_dict and o['SYMBOL'] != "":
            sym_dict[primary_key] = o['SYMBOL']



with open(os.path.expanduser(old_file), "r") as f:
    reader = csv.DictReader(f, delimiter='\t')

    old_keys = set()
    old_count = 0

    for o in reader:
        if o['PROJECT'] in skipped_projects:
            continue
        old_count += 1
        primary_key = ":".join([o['GENE'], o['PROJECT']])
        old_keys.add(primary_key)

        old_clust = o[CLUST_OLD]
        old_fm = o[FM_OLD]

        if 'SYMBOL' in o and o['SYMBOL'] not in sym_dict and o['SYMBOL'] != "":
            sym_dict[primary_key] = o['SYMBOL']

        if not primary_key in new_genes:
            new_clust = new_fm = "-"
        else:
            n = new_genes[primary_key]
            new_clust = n[CLUST_NEW]
            new_fm = n[FM_NEW]



        if not only_low_ps or below_threshold([old_clust, new_clust], P):
            if diff_double(old_clust, new_clust, delta=0.01):
                print("At {} ({}) CLUST-Pval was #{}# and now is #{}#".format(primary_key, get_sym(primary_key), old_clust, new_clust))
                different_count['CLUST'] += 1
            else:
                similar_count['CLUST'] += 1
                #print("At {} ({}) CLUST-Pvals are similar: #{}# VS #{}#".format(primary_key, get_sym(primary_key), old_clust, new_clust))


        if not only_low_ps or below_threshold([old_fm, new_fm], P):
            if diff_double(old_fm, new_fm, delta=0.01):
                print("At {} ({}) FM-Pval was #{}# and now is #{}#".format(primary_key, get_sym(primary_key), old_fm, new_fm))
                different_count['FM'] += 1
            else:
                similar_count['FM'] += 1

for k in new_genes.keys():

    if not k in old_keys:
        print("{0} not found in old file".format(k))

print("Total new entries: {0}".format(new_count))
print("Total old entries: {0}".format(old_count))
print("Total duplicates: {0}".format(duplicates_count))
print("Total different: {0}".format(different_count))
print("Total similar: {0}".format(similar_count))




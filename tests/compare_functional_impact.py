import csv
import os
from intogen.constants.data_headers import CHR, STRAND, START, REF, ALT, TRANSCRIPT, TRANSCRIPT_IMPACT, MA_CLASS, \
    MA_TRANSFIC, MA_SCORE, TRANSCRIPT_IMPACT_CLASS, PPH2_CLASS, PPH2_TRANSFIC, PPH2_SCORE, SIFT_CLASS, SIFT_TRANSFIC, \
    SIFT_SCORE, AA_CHANGE, PROTEIN_POS, PROTEIN, UNIPROT, GENE, CONSEQUENCE
from intogen.utils import CommentedFile
from tests.compare import diff_str, diff_sets, diff_double

new_file = "~/intogen5/bin/output/PRAD_broad/sample_variant+transcript.impact"
old_file = "~/intogen5/examples/projects/PRAD_broad/wok-output/results/consequence.tsv"


new_variants = {}
with open(os.path.expanduser(new_file), "r") as f:
    reader = csv.DictReader(CommentedFile(f), delimiter='\t')

    new_count = 0
    duplicates_count = 0
    for o in reader:
        new_count += 1
        variant_key = ":".join([o[CHR], o[STRAND], o[START], o[REF]+'/'+o[ALT], o[TRANSCRIPT]])

        if variant_key in new_variants:
            # print("Duplicated {0} at sample {1} previous {2}".format(variant_key, o['sample'], new_variants[variant_key]['sample']))
            duplicates_count += 1

        new_variants[variant_key] = o

with open(os.path.expanduser(old_file), "r") as f:
    reader = csv.DictReader(CommentedFile(f), delimiter='\t')

    old_keys = set()
    old_count = 0
    count_new_uniprots = 0
    count_old_uniprots = 0
    count_new_proteins = 0
    count_new_protein_pos = 0
    count_new_aa_change = 0

    for o in reader:
        old_count += 1
        variant_key = ":".join([o['CHR'], o['STRAND'], o['START'], o['ALLELE'], o['TRANSCRIPT_ID']])
        old_keys.add(variant_key)

        if not variant_key in new_variants:
            print("{0} not found in new file".format(variant_key))
            continue

        n = new_variants[variant_key]

        #
        #
        # IMPACT	IMPACT_CLASS
        # tr_impact prot_change coding_region

        if diff_sets(o['CT'], n[CONSEQUENCE]):
            print("At {0} consequence was #{1}# and now is #{2}#".format(variant_key, o['CT'], n[CONSEQUENCE]))

        if diff_str(o['GENE_ID'], n[GENE]):
            print("At {0} gene was #{1}# and now is #{2}#".format(variant_key, o['GENE_ID'], n[GENE]))

        # Protein
        if diff_str(o['UNIPROT_ID'], n[UNIPROT]):
            if o['UNIPROT_ID'] == "\\N" and n[UNIPROT] != "":
                count_new_uniprots += 1
            elif o['UNIPROT_ID'] != "\\N" and n[UNIPROT] == "":
                count_old_uniprots += 1
            else:
                print("At {0} uniprot was #{1}# and now is #{2}#".format(variant_key, o['UNIPROT_ID'], n[UNIPROT]))

        if diff_str(o['PROTEIN_ID'], n[PROTEIN]):
            if o['PROTEIN_ID'] == "\\N" and n[PROTEIN] != "":
                count_new_proteins += 1
            else:
                print("At {0} protein was #{1}# and now is #{2}#".format(variant_key, o['PROTEIN_ID'], n[PROTEIN]))

        if diff_str(o['PROTEIN_POS'], n[PROTEIN_POS]):
            if o['PROTEIN_POS'] == "\\N" and n[PROTEIN_POS] != "":
                count_new_protein_pos += 1
            else:
                print("At {0} protein_pos was #{1}# and now is #{2}#".format(variant_key, o['PROTEIN_POS'], n[PROTEIN_POS]))

        if diff_str(o['AA_CHANGE'], n[AA_CHANGE]):
            if o['AA_CHANGE'] == "\\N" and n['aa_change'] != "":
                count_new_aa_change += 1
            else:
                print("At {0} aa_change was #{1}# and now is #{2}#".format(variant_key, o['AA_CHANGE'], n[AA_CHANGE]))

        # SIFT
        if diff_double(o['SIFT_SCORE'], n[SIFT_SCORE]):
            print("At {0} sift_score was #{1}# and now is #{2}#".format(variant_key, o['SIFT_SCORE'], n[SIFT_SCORE]))

        if diff_double(o['SIFT_TRANSFIC'], n[SIFT_TRANSFIC]):
            print("At {0} sift_tfic was #{1}# and now is #{2}#".format(variant_key, o['SIFT_TRANSFIC'], n[SIFT_TRANSFIC]))

        if diff_str(o['SIFT_TRANSFIC_CLASS'], n[SIFT_CLASS]):
            print("At {0} sift_class was #{1}# and now is #{2}#".format(variant_key, o['SIFT_TRANSFIC_CLASS'], n[SIFT_CLASS]))

        # PPH2
        if diff_double(o['PPH2_SCORE'], n[PPH2_SCORE]):
            print("At {0} pph2_score was #{1}# and now is #{2}#".format(variant_key, o['PPH2_SCORE'], n[PPH2_CLASS]))

        if diff_double(o['PPH2_TRANSFIC'], n[PPH2_TRANSFIC]):
            print("At {0} pph2_tfic was #{1}# and now is #{2}#".format(variant_key, o['PPH2_TRANSFIC'], n[PPH2_TRANSFIC]))

        if diff_str(o['PPH2_TRANSFIC_CLASS'], n[PPH2_CLASS]):
            print("At {0} pph2_class was #{1}# and now is #{2}#".format(variant_key, o['PPH2_TRANSFIC_CLASS'], n[PPH2_CLASS]))

        # MA
        if diff_double(o['MA_SCORE'], n[MA_SCORE]):
            print("At {0} ma_score was #{1}# and now is #{2}#".format(variant_key, o['MA_SCORE'], n[MA_SCORE]))

        if diff_double(o['MA_TRANSFIC'], n[MA_TRANSFIC]):
            print("At {0} ma_tfic was #{1}# and now is #{2}#".format(variant_key, o['MA_TRANSFIC'], n[MA_TRANSFIC]))

        if diff_str(o['MA_TRANSFIC_CLASS'], n[MA_CLASS]):
            print("At {0} ma_class was #{1}# and now is #{2}#".format(variant_key, o['MA_TRANSFIC_CLASS'], n[MA_CLASS]))

        # Impact
        if diff_double(o['IMPACT'], n[TRANSCRIPT_IMPACT]):
            print("At {0} tr_impact was #{1}# and now is #{2}#".format(variant_key, o['IMPACT'], n[TRANSCRIPT_IMPACT]))

        if diff_str(o['IMPACT_CLASS'], n[TRANSCRIPT_IMPACT_CLASS]):
            print("At {0} tr_impact_class was #{1}# and now is #{2}#".format(variant_key, o['IMPACT_CLASS'], n[TRANSCRIPT_IMPACT_CLASS]))

for k in new_variants.keys():

    if not k in old_keys:
        print("{0} not found in old file".format(k))

print("Total new entries: {0}".format(new_count))
print("Total old entries: {0}".format(old_count))
print("Total duplicates: {0}".format(duplicates_count))
print("Total new uniprots: {0}".format(count_new_uniprots))
print("Total missing uniprots: {0}".format(count_old_uniprots))
print("Total new protein_id: {0}".format(count_new_proteins))
print("Total new protein_pos: {0}".format(count_new_protein_pos))
print("Total new aa_change: {0}".format(count_new_aa_change))




# IntOGen pipeline #

## License ##
IntOGen is made available to the general public subject to certain conditions described in its [LICENSE](LICENSE). 
For the avoidance of doubt, you may use the software and any data accessed through UPF software for academic, 
non-commercial and personal use only, and you may not copy, distribute, transmit, duplicate, reduce or alter in 
any way for commercial purposes, or for the purpose of redistribution, without a license from the Universitat 
Pompeu Fabra (UPF). Requests for information regarding a license for commercial use or redistribution of 
IntOGen pipeline may be sent via e-mail to innovacio@upf.edu.

## Requirements ##

IntOGen depends on **Python 3.4** or above and some python libraries. If you don't have Python 3.4 already installed, the easiest way to install all this software stack is using the well known [Anaconda Python distribution](http://continuum.io/downloads#34).
 
Also **Perl 5.10** (with DBI module installed) or above has to be available at PATH to be able to run VEP scripts.
  
By default MutsigCV is disabled. If you want to enable it you have to first download and install 
[Matlab Runtime](http://es.mathworks.com/products/compiler/mcr/) and [MutsigCV](https://www.broadinstitute.org/cancer/cga/mutsig) 
and then edit the IntOGen configuration file that by default it's at ~/.intogen/system.conf 
(parameters: mutsig_enabled, mutsig_path and matlab_mcr) 

## Installation ##

To install or update to the last stable version of IntOGen you need to run: 

    $ pip install intogen pandas=0.17

After this you will have the `intogen` script available at your path and if this is the first time that you install IntOGen you need to run the setup to download all the data dependencies. This setup will download ~3.6Gb of data that after uncompress it will need ~9Gb of free space. 

    $ intogen --setup
    
**TIP**: By default the IntOGen configuration files are in `~/.intogen` if you want to change this folder you need to define
the system environment variable **INTOGEN_HOME** using the `export` command. Also, all the datasets are downloaded by
default at `~/.bgdata` if you want to change this folder you need to define the system environment variable **BGDATA_LOCAL**.

## Run an example ##

Download and extract some samples VCF files:

    $ wget https://bitbucket.org/intogen/intogen-pipeline/downloads/intogen-samples.tar.gz
    $ tar xvzf intogen-samples.tar.gz
    
Run IntOGen using the default tasks configuration.

    $ intogen -i sample1.vcf -i sample2.vcf -i sample3.vcf -i sample4.vcf
    
Browse the results at the `output` folder.
 
## Custom configuration ##

At `~/.intogen/task.conf` you can check the default task configuration values. If you want to run the pipeline 
using different parameters you can change the default values or create a `.smconfig` file for each project. 

The `.smconfig` files are a copy of `~/.intogen/task.conf` but adding `id` and `files` parameters. The `id` is the name 
of the project and the `files` is a list separated by comma of all the files (MAF, VCF or tab format) that contain 
samples for that project. 

You can create a `.smconfig` file like this:
 
    $ echo -e "id = allsamples\nfiles = sample1.vcf,sample2.vcf,sample3.vcf,sample4.vcf\n" > allsamples.smconfig
    $ cat ~/.intogen/task.conf >> allsamples.smconfig

To run it again, you need to delete or move the previous output and run using the `.smconfig` file as input.

    $ rm -rf output
    $ intogen -i allsamples.smconfig
    
If you want to run multiple projects at once you can create multiple `.smconfig` files in one folder and then give that
folder as input.